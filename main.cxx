#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <map>


#include <vtkm/cont/DeviceAdapter.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleCounting.h>
#include <vtkm/cont/ArrayHandlePermutation.h>

#include <vtkm/cont/DynamicArrayHandle.h>
#include <vtkm/cont/Timer.h>
#include <vtkm/Pair.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/ArrayHandleCompositeVector.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>

#include "cp_time.h"

using namespace std;

template <typename T>
void equal(const T &v1, const T &v2, const char *msg)
{
    if ( v1 - v2 > 1e-7 && v1 - v2 < -1e-7) {
        cout << "Assersion Fail: " << msg << "-- v1, v2 = " << v1 << "," << v2 << endl;
        exit(1);
    }
}

template <typename T>
vtkm::cont::ArrayHandle<T> genValues(int n, int max)
{
    T *v = new T[n];
    for (int i=0; i<n; i++)
    {
        v[i] = rand() * (double)max / RAND_MAX ;
    }
    auto a = vtkm::cont::make_ArrayHandle<T>(v, n);
    return a;
}

template <typename T>
void compare( vtkm::cont::ArrayHandle<T> array1, vtkm::cont::ArrayHandle<T> array2 )
{
    equal( array1.GetNumberOfValues() , array2.GetNumberOfValues(), "reduced length not equal");
    for (int i = 0; i< array1.GetNumberOfValues(); i++)
    {
        equal( array1.GetPortalConstControl().Get(i), array2.GetPortalConstControl().Get(i), "value different");
        //cout << array1.GetPortalConstControl().Get(i) << " ";
    }
    //cout << endl;
}

typedef vtkm::cont::DeviceAdapterAlgorithm<vtkm::cont::DeviceAdapterTagSerial> SerialAlgorithm ;
typedef vtkm::cont::DeviceAdapterAlgorithm<vtkm::cont::DeviceAdapterTagTBB> TBBAlgorithm;

template <class T>
void test(int len, int max_diff_keys, bool sort_keys)
{
    auto keys = genValues<int>(len, max_diff_keys);
    auto values = genValues<T>(len, 100);

    vtkm::cont::ArrayHandle<int> keysReducedSerial;
    vtkm::cont::ArrayHandle<T> valuesReducedSerial;

    vtkm::cont::ArrayHandle<int> keysReducedTBB;
    vtkm::cont::ArrayHandle<T> valuesReducedTBB;

    if (sort_keys)
        SerialAlgorithm::Sort(keys);

    // serial
    SerialAlgorithm::ReduceByKey(keys, values, keysReducedSerial, valuesReducedSerial, vtkm::internal::Add());
    // TBB
    TBBAlgorithm::ReduceByKey(keys, values, keysReducedTBB, valuesReducedTBB, vtkm::internal::Add());

    // compare
    cout << "Checking keys" << endl;
    compare(keysReducedSerial, keysReducedTBB);
    cout << "Checking values" << endl;
    compare(valuesReducedSerial, valuesReducedTBB);


}


int main()
{
    srand(Timer::getTimeMS());

    const int cases = 100;
    for (int c = 0; c<cases; c++)
    {
        cout << "====================================  test #" << c << endl;
        int len = rand() % 10000 +1;
        int max_diff_keys = rand() % len + 1;
        bool sort = false;
        cout << "len = " << len << ", max keys = " << max_diff_keys << endl;
        test<int>(len, max_diff_keys, sort);
    }

    for (int c = 0; c<cases; c++)
    {
        cout << "====================================  test #" << c << endl;
        int len = rand() % 10000 +1;
        int max_diff_keys = rand() % len + 1;
        bool sort = true;
        cout << "len = " << len << ", max keys = " << max_diff_keys << endl;
        test<int>(len, max_diff_keys, sort);
    }

    for (int c = 0; c<cases; c++)
    {
        cout << "====================================  test #" << c << endl;
        int len = rand() % 10000 +1;
        int max_diff_keys = rand() % len + 1;
        bool sort = true;
        cout << "len = " << len << ", max keys = " << max_diff_keys << endl;
        test<float>(len, max_diff_keys, sort);
    }
    cout << "No difference." << endl;


    return 0;
}
